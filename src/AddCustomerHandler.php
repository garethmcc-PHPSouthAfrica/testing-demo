<?php

namespace Raines\Serverless;

use Aws\DynamoDb\DynamoDbClient;
use Ramsey\Uuid\Uuid;
use Aws\Sdk;


class AddCustomerHandler implements Handler
{
    /**
     * {@inheritdoc}
     */
    public function handle(array $event, Context $context)
    {
        $logger = $context->getLogger();
        $requestBody = json_decode($event['body']);
        $sdk = new Sdk([
            'region'  => 'us-east-1',
            'version' => 'latest'
        ]);
        $dynamoDbClient = $sdk->createDynamoDb();
        // $dynamoDbClient = new DynamoDbClient([]);
        $itemData = [
            'uuid' => Uuid::uuid4()->toString(),
            'email' => $requestBody->email,
        ];
        try {
            $putResult = $dynamoDbClient->putItem([
                'TableName' => $_ENV['DYNAMODB_CUSTOMERTABLENAME'],
                'Item' => [
                    'uuid'=> [
                        'S' => $itemData['uuid'],
                    ],
                    'email'=> [
                        'S' => $itemData['email'],
                    ],
                ]
            ]);
            $snsClient = $sdk->createSns();
            $publishParams = [
                'Message'=> json_encode($itemData),
                'Subject'=>'CreateCustomer',
                'TopicArn'=>$_ENV['SNS_CREATECUSTOMER_TOPICARN']
            ];
            $logger->notice('$publishParams', $publishParams);
            $snsClient->publish($publishParams);

            return [
                'statusCode' => 201,
                'body' => json_encode($itemData),
            ];
        } catch (Exception $exception) {
            return [
                'statusCode' => 500,
                'body' => 'There was an error dealing with the put',
            ];
        }
    }
}
