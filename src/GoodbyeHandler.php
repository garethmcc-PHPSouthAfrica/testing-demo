<?php

namespace Raines\Serverless;

class GoodbyeHandler implements Handler
{
    /**
     * {@inheritdoc}
     */
    public function handle(array $event, Context $context)
    {
        $logger = $context->getLogger();
        $logger->notice('Got event', $event);

        return [
            'statusCode' => 200,
            'body' => 'Goodbye',
        ];
    }
}
