<?php

namespace Raines\Serverless;

use Aws\Sdk;

class UpdateCustomerHandler implements Handler
{
    /**
     * {@inheritdoc}
     */
    public function handle(array $event, Context $context)
    {
        $logger = $context->getLogger();
        $logger->notice('Got event', $event);
        $requestBody = json_decode($event['body']);
        if (!$requestBody) {
            $logger->error('JSON could not be decoded correctly');
            $logger->info('$event->body', [$event['body']]);
            return [
                'statusCode' => 400
            ];
        }
        $sdk = new Sdk([
            'region'=>'us-east-1',
            'version'=>'latest'
        ]);
        $updateItemParams = [
            'TableName' => $_ENV['DYNAMODB_CUSTOMERTABLENAME'],
            'Key' => [
              'uuid' => [
                  'S' => $event['pathParameters']['customerUuid']
              ]
            ],
            'ExpressionAttributeNames' => [
                '#email' => 'email'
            ],
            'ExpressionAttributeValues' => [
                ':email' => [
                    'S' => $requestBody->email
                ]
            ],
            'UpdateExpression' => 'SET #email = :email'
        ];
        $logger->notice('$updateItemParams', [$updateItemParams]);
        $dynamoDb = $sdk->createDynamoDb();
        $dynamoDb->updateItem($updateItemParams);
        return [
            'statusCode' => 200,
            'body' => 'Go Serverless v1.0! Your function executed successfully!',
        ];
    }
}
