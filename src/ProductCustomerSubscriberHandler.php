<?php

namespace Raines\Serverless;

use Aws\Sdk;

class ProductCustomerSubscriberHandler implements Handler
{
    /**
     * {@inheritdoc}
     */
    public function handle(array $event, Context $context)
    {
        $logger = $context->getLogger();
        $logger->notice('Got event', $event);
        $eventMessage = json_decode($event['Records'][0]['Sns']['Message']);
        $eventSubject = $event['Records'][0]['Sns']['Subject'];
        if ($eventSubject !== 'CustomerUpdated') {
            $logger->alert('Message not meant for me');
            $logger->info('$eventSubject $eventMessage', [
                $eventSubject,
                $eventMessage
            ]);
            throw new \Error('Subject did not match what was required', 0);
        }
        $sdk = new Sdk([
            'region' => 'us-east-1',
            'version'=>'latest'
        ]);

        return [
            'statusCode' => 200,
            'body' => 'Go Serverless v1.0! Your function executed successfully!',
        ];
    }
}
