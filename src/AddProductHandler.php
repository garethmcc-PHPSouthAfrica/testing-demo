<?php

namespace Raines\Serverless;

use Aws\Sdk;
use Ramsey\Uuid\Uuid;

class AddProductHandler implements Handler
{
    /**
     * {@inheritdoc}
     */
    public function handle(array $event, Context $context)
    {
        $logger = $context->getLogger();
        $logger->notice('event', $event);
        $sdk = new Sdk([
            'region' => 'us-east-1',
            'version' => 'latest'
        ]);
        $requestBody = json_decode($event->Records[0]->Sns->Message);
        $dynamoDb = $sdk->createDynamoDb();
        $putParams = [
            'TableName'=> $_ENV['DYNAMODB_PRODUCTTABLENAME'],
            'Item' => [
                'uuid' => [
                    'S' => Uuid::uuid4()->toString(),
                ],
                'productName' => [
                    'S' => $requestBody->productName,
                ],
                'productDescription' => [
                    'S' => $requestBody->productDescription
                ],
                'customer'=> [
                    'M' => [
                        'uuid' => [
                            'S' => $requestBody->customer->uuid,
                        ],
                        'email' => [
                            'S' => $requestBody->customer->email
                        ]
                    ]
                ],
                'customerUuid'=> [
                    'S' => $requestBody->customer->uuid
                ]
            ]
        ];
        try {
            $putResult = $dynamoDb->putItem($putParams);
            return [
                'statusCode' => 200,
                'body' => json_encode([
                    'uuid'=>$putParams['Item']['uuid']['S']
                ])
            ];
        } catch (Exception $exception) {
            $logger->notice('There was an error putting product details');
            $logger->critical('exception', [$exception]);
            return [
                'statusCode' => 500,
            ];
        }
    }
}
